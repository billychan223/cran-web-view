import mongoose, { Schema } from "mongoose";

const PackageSchema = new Schema({
  packageName: { type: String, required: true },
  type: { type: String, required: false },
  version: { type: String, required: true },
  title: { type: String, required: true },
  author: { type: String, required: true },
  maintainer: { type: String, required: false },
  description: { type: String, required: true },
  license: { type: String, required: true },
  date: { type: String, required: false },
  depends: { type: String, required: false },
  suggests: { type: String, required: false },
  needsCompilation: { type: String, required: false },
  packaged: { type: String, required: false },
  repository: { type: String, required: false },
  datePublication: { type: String, required: false },
});
const cranPackage = mongoose.model("Package", PackageSchema);
export default cranPackage;
