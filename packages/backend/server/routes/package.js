import express from "express";
import packageSchema from "../models/package.model";

const router = express.Router();

router.get("/", async function(req, res, next) {
  const { limit = 10, skip = 0, author } = req.query;
  const searchQuery = author
    ? { author: new RegExp(`/.*${author}.*/`, "i") }
    : {};
  const cranPackages = await packageSchema
    .find(searchQuery, { _id: 0, __v: 0 })
    .skip(parseInt(skip, 10))
    .limit(parseInt(limit, 10))
    .lean();
  const total = await packageSchema.count(searchQuery);
  res.send({ data: cranPackages, total });
});

router.get("/:name/:version", async function(req, res, next) {
  const { name, version } = req.params;
  const cranPackage = await packageSchema.findOne({
    packageName: name,
    version: version,
  });
  if (!cranPackage) {
    res.status(404).send("CRAN package Not found");
  } else {
    res.send(cranPackage);
  }
});

export default router;
