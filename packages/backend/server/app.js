// app.js
import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";
import indexRouter from "./routes/index";
import packagesRouter from "./routes/package";
import MongoConnector from "./connectors/MongoConnector";
import cors from "cors";

const app = express();
app.use(logger("dev"));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "../public")));
app.use("/", indexRouter);
app.use("/packages", packagesRouter);
MongoConnector.getInstance()
  .connect()
  .catch((e) => console.error(e));

export default app;
