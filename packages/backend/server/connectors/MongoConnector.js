import Mongoose from "mongoose";

class MongoConnector {
  static instance;

  constructor() {}

  static getInstance() {
    return this.instance || (this.instance = new this());
  }

  async connect() {
    await Mongoose.connect("mongodb://localhost:27017/package", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }
}

export default MongoConnector;
