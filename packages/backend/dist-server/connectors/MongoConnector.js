"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _mongoose = _interopRequireDefault(require("mongoose"));

var MongoConnector = /*#__PURE__*/function () {
  function MongoConnector() {
    (0, _classCallCheck2["default"])(this, MongoConnector);
  }

  (0, _createClass2["default"])(MongoConnector, [{
    key: "connect",
    value: function () {
      var _connect = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _mongoose["default"].connect("mongodb://localhost:27017/package", {
                  useNewUrlParser: true,
                  useUnifiedTopology: true
                });

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function connect() {
        return _connect.apply(this, arguments);
      }

      return connect;
    }()
  }], [{
    key: "getInstance",
    value: function getInstance() {
      return this.instance || (this.instance = new this());
    }
  }]);
  return MongoConnector;
}();

(0, _defineProperty2["default"])(MongoConnector, "instance", void 0);
var _default = MongoConnector;
exports["default"] = _default;