"use strict";

var _typeof = require("@babel/runtime/helpers/typeof");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireWildcard(require("mongoose"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var PackageSchema = new _mongoose.Schema({
  packageName: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: false
  },
  version: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  author: {
    type: String,
    required: true
  },
  maintainer: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: true
  },
  license: {
    type: String,
    required: true
  },
  date: {
    type: String,
    required: false
  },
  depends: {
    type: String,
    required: false
  },
  suggests: {
    type: String,
    required: false
  },
  needsCompilation: {
    type: String,
    required: false
  },
  packaged: {
    type: String,
    required: false
  },
  repository: {
    type: String,
    required: false
  },
  datePublication: {
    type: String,
    required: false
  }
});

var cranPackage = _mongoose["default"].model("Package", PackageSchema);

var _default = cranPackage;
exports["default"] = _default;