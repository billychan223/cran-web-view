"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _express = _interopRequireDefault(require("express"));

var _package = _interopRequireDefault(require("../models/package.model"));

var router = _express["default"].Router();

router.get("/", /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res, next) {
    var _req$query, _req$query$limit, limit, _req$query$skip, skip, author, searchQuery, cranPackages, total;

    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _req$query = req.query, _req$query$limit = _req$query.limit, limit = _req$query$limit === void 0 ? 10 : _req$query$limit, _req$query$skip = _req$query.skip, skip = _req$query$skip === void 0 ? 0 : _req$query$skip, author = _req$query.author;
            searchQuery = author ? {
              author: new RegExp("/.*".concat(author, ".*/"), "i")
            } : {};
            _context.next = 4;
            return _package["default"].find(searchQuery, {
              _id: 0,
              __v: 0
            }).skip(parseInt(skip, 10)).limit(parseInt(limit, 10)).lean();

          case 4:
            cranPackages = _context.sent;
            _context.next = 7;
            return _package["default"].count(searchQuery);

          case 7:
            total = _context.sent;
            res.send({
              data: cranPackages,
              total: total
            });

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}());
router.get("/:name/:version", /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res, next) {
    var _req$params, name, version, cranPackage;

    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _req$params = req.params, name = _req$params.name, version = _req$params.version;
            _context2.next = 3;
            return _package["default"].findOne({
              packageName: name,
              version: version
            });

          case 3:
            cranPackage = _context2.sent;

            if (!cranPackage) {
              res.status(404).send("CRAN package Not found");
            } else {
              res.send(cranPackage);
            }

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
}());
var _default = router;
exports["default"] = _default;