import Crawler from "crawler";
import { Package } from "../models/Package";
import { descriptionCrawler } from "./descriptionCrawler";

const parsePackage = (data: string) => {
  data = data.replace(/\n        /g, " ");
  const lines = data.split("\n");
  const p: Package = {};

  lines.forEach((line) => {
    const [type, value] = line.split(": ");
    switch (type) {
      case "Package": {
        p.packageName = value;
        break;
      }
      case "Version": {
        p.version = value;
        break;
      }
      default: {
        break;
      }
    }
  });
  const sourceUrl = `https://cran.r-project.org/src/contrib/${p.packageName}_${p.version}.tar.gz`;
  const filename = sourceUrl.split("/").reverse()[0];
  console.log(sourceUrl);
  descriptionCrawler.queue({
    uri: sourceUrl,
    filename: filename,
  });
  return p;
};

const crawlerCallback = (
  err: Error,
  res: Crawler.CrawlerRequestResponse,
  done: () => void
) => {
  if (err) {
    console.log("err: " + err);
  } else {
    const body = res.body.toString();
    const data = body.split("\n\n");
    const packages: Package[] = data.map(parsePackage);
    console.log(packages.length);
  }
  done();
};

export const mainCrawler = new Crawler({
  maxConnections: 10,
  jQuery: false,
  callback: crawlerCallback,
});
