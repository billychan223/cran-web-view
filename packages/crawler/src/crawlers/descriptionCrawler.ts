import Crawler from "crawler";
import fs from "fs";
import { extractDescription } from "../utils/extractDescription";

const dir = "./tmp/";

const onFileDownloaded = (filepath: string, err?: Error | null) => {
  if (err) {
    console.log(err);
    return;
  }
  extractDescription(filepath);
  console.log(`Downloaded ${filepath}`);
};

const crawlerCallback = (
  err: Error,
  res: Crawler.CrawlerRequestResponse,
  done: () => void
) => {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  if (err) {
    console.error(err.stack);
  } else {
    const filepath: string = dir + res.options.filename;
    fs.createWriteStream(filepath).write(res.body, (err) => {
      onFileDownloaded(filepath, err);
    });
  }

  done();
};

export const descriptionCrawler = new Crawler({
  encoding: null,
  maxConnections: 10,
  jQuery: false,
  gzip: true,
  callback: crawlerCallback,
});
