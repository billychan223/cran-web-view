import tar from "tar-stream";
import fs from "fs";
import zlib from "zlib";
import parseDescription from "./parseDescription";
import MongoConnector from "./MongoConnector";

export const extractDescription = (filepath: string) => {
  var extract = tar.extract();
  var data = "";
  const folderName = filepath
    .split("/")
    .reverse()[0]
    .split("_")[0];
  const filePrefix = filepath.split("/").reverse()[0] + "_";
  const dir = "./tmp/descriptions/";

  extract.on("entry", function(header, stream, cb) {
    stream.on("data", function(chunk) {
      if (header.name == folderName + "/DESCRIPTION") data += chunk;
    });

    stream.on("end", function() {
      cb();
    });

    stream.resume();
  });

  extract.on("finish", function() {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    fs.writeFile(dir + filePrefix + "DESCRIPTION", data, () => {
      const p = parseDescription(dir + filePrefix + "DESCRIPTION");

      MongoConnector.getInstance().upsert(p);

      fs.unlink(filepath, () => {
        console.log(filepath + "removed");
      });
    });
  });

  fs.createReadStream(filepath)
    .pipe(zlib.createGunzip())
    .pipe(extract);
};
