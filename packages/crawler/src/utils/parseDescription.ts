import fs from "fs";
import { Package } from "../models/Package";

const parseDescription = (filepath: string) => {
  var data = fs.readFileSync(filepath, "utf8");
  data = data
    .replace(/\n        /g, " ")
    .replace(/\n    /g, " ")
    .replace(/\n  /g, " ");
  const p: Package = {};
  data.split("\n").forEach((line) => {
    const [type, value] = line.split(": ");
    switch (type) {
      case "Package": {
        p.packageName = value;
        break;
      }
      case "Type": {
        p.type = value;
        break;
      }
      case "Version": {
        p.version = value;
        break;
      }
      case "Date": {
        p.date = value;
        break;
      }
      case "Title": {
        p.title = value;
        break;
      }
      case "Author": {
        p.author = value;
        break;
      }
      case "Maintainer": {
        p.maintainer = value;
        break;
      }
      case "Description": {
        p.description = value;
        break;
      }
      case "License": {
        p.license = value;
        break;
      }
      case "Depends": {
        p.depends = value;
        break;
      }
      case "Suggests": {
        p.suggests = value;
        break;
      }
      case "NeedsCompilation": {
        p.needsCompilation = value;
        break;
      }
      case "Packaged": {
        p.packaged = value;
        break;
      }
      case "Repository": {
        p.repository = value;
        break;
      }
      case "Date/Publication": {
        p.datePublication = value;
        break;
      }
      default: {
        // console.log(type);
        break;
      }
    }
  });

  fs.unlink(filepath, () => {
    console.log(filepath + "removed");
  });

  return p;
};

export default parseDescription;
