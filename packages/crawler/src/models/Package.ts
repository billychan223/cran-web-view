import { model, Schema, Model, Document } from "mongoose";

interface Package {
  packageName?: string;
  type?: string;
  version?: string;
  title?: string;
  author?: string;
  maintainer?: string;
  description?: string;
  license?: string;
  date?: string;
  depends?: string;
  suggests?: string;
  needsCompilation?: string;
  packaged?: string;
  repository?: string;
  datePublication?: string;
}

interface PackageDocument extends Package, Document {}

const PackageSchema: Schema = new Schema({
  packageName: { type: String, required: true },
  type: { type: String, required: false },
  version: { type: String, required: true },
  title: { type: String, required: true },
  author: { type: String, required: true },
  maintainer: { type: String, required: false },
  description: { type: String, required: true },
  license: { type: String, required: true },
  date: { type: String, required: false },
  depends: { type: String, required: false },
  suggests: { type: String, required: false },
  needsCompilation: { type: String, required: false },
  packaged: { type: String, required: false },
  repository: { type: String, required: false },
  datePublication: { type: String, required: false },
});

const Package: Model<PackageDocument> = model("package", PackageSchema);

export { Package, PackageDocument, PackageSchema };
