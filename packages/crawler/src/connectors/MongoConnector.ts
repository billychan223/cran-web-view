import Mongoose from "mongoose";
import { Package } from "../models/Package";

class MongoConnector {
  private static instance: MongoConnector;
  constructor() {}

  public static getInstance() {
    return this.instance || (this.instance = new this());
  }

  async connect() {
    await Mongoose.connect("mongodb://localhost:27017/package", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }

  async upsert(p: Package) {
    try {
      if (Mongoose.connection.readyState !== Mongoose.STATES.connected) {
        await this.connect();
      }
      const pd = await Package.findOneAndUpdate(
        { version: p.version, packageName: p.packageName },
        p,
        {
          new: true,
          upsert: true,
        }
      ).lean();
      console.log(`${pd.packageName} with version ${pd.version} has upserted`);
    } catch (e) {
      console.error(e);
    }
  }
}

export default MongoConnector;
