import { useQuery } from "react-query";
import axios from "axios";

const getPackages = async (page, pageSize, author) => {
  const query =
    author && author !== ""
      ? { skip: (page - 1) * pageSize, limit: pageSize, author }
      : { skip: (page - 1) * pageSize, limit: pageSize };
  const { data } = await axios.get("http://localhost:3000/packages", {
    params: query,
  });

  return data;
};

export default function usePackages(page, pageSize, author) {
  return useQuery(
    ["packages", page, pageSize, author],
    () => getPackages(page, pageSize, author),
    { keepPreviousData: true, staleTime: Infinity }
  );
}
