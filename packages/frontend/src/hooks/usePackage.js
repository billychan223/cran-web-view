import { useQuery } from "react-query";
import axios from "axios";

const getPostByName = async (packageName, version) => {
  const { data } = await axios.get(
    `localhost:3000/packages/${packageName}/version/${version}`
  );
  return data;
};

export default function usePackages(packageName, version) {
  return useQuery(["package", packageName], () =>
    getPostByName(packageName, version)
  );
}
