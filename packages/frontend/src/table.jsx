import * as React from 'react';
import { useState } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import TextField from '@material-ui/core/TextField';

import usePackages from './hooks/usePackages';

function Table() {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [author, setAuthor] = useState('');
  const {
    isLoading, data,
  } = usePackages(page, pageSize, author);
  let tableData = [];
  if (data) {
    tableData = data.data.map((value) => ({ ...value, id: `${value.packageName}_${value.version}` }));
  }
  const columns = [
    { field: 'packageName', headerName: 'Package Name', width: 300 },
    {
      field: 'description', headerName: 'Description', width: 300,
    },
    { field: 'version', headerName: 'version', width: 300 },
  ];

  return (isLoading ? 'isLoading'
    : (
      <>
        <TextField id="standard-search" label="Search field" type="search" value={author} onChange={(event) => setAuthor(event.target.value)} />
        <div style={{ height: 400, width: '100%' }}>
          <DataGrid paginationMode="server" rows={tableData} rowCount={data.total} columns={columns} pageSize={pageSize} rowsPerPageOptions={[5, 10, 20]} pagination onPageChange={(param) => { setPage(param.page); setPageSize(param.pageSize); }} />
        </div>
      </>
    )
  );
}

export default Table;
