# CRAN-Web-View

## Prerequisite

1. spin up mongo first
```
cd packages/backend
docker-compose up -d 
```
## How to run all

1. run in root folder

``` 
yarn start:dev
```
## Backend

### Scope

1. only two endpoints which is Get Many and Get One package

### Things to Do

1. Add request response DTO
2. Add proper error handling
3. May Transform to typescript
4. Add integration test and unit test using jest

### How to run backend 

```
yarn start:dev 
```
for monitoring files changes and restart server

## frontend

### Scope

1. table and search, no time for version grouping


### Things to do

1. error handling
2. add style to page
3. add routing
4. add specific page for version detail 
5. add test

## Crawler

### Scope

1. crawl once and parse once

### things to do

1. scheduled job/ cron job
2. retry logic/error handling
3. multiple crawlers?
4. add unit test

